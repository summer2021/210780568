### 在dubbo3,0 基础上进行的修改。

🍊添加的模块：

​    ---- dubbo-api-docs-annotatins

**🍊添加的**demo：

-------- dubbo-demo

​          -- -----dubbo-api-docs-examples

🍊改动的代码：

​      --------dubbo-metadata

​             ---------dubbo-metadata-api

###	项目进度

🍊1.1学习 dubbo-api-docs

🍊1.2 学习dubbo 的 SPI 扩展机制

🍊1.3 学习元数据中心

🍊1.4学习dubbo 服务暴露过程

🍊1.5 学习dubbo admin

🍊1.6 api docs 和dubbo admin交互

🍊1.7 元数据中心如何和dubbo admin交互

🍊1.8 分析metadata和docs信息格式

🍊1.9 研究metadata和docs生成过程

🍊1.10分析dubbo admin 获取metadata和docs的代码

🍊1.11 研究服务查询和接口文档中对接口进行测试的代码...

🍊1.12创建demo 将api-docs-demo移动到 dubbo3.0.

🍊1.13将apidocs核心代码挪动到元数据中心...

🍊1.14 学习dubbo 3.0 云原生... 

🍊1.15 测试方案，产出文档





## 1.1学习 dubbo-api-docs

Docs生成大流程

![img](https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images/clip_image002-1632657096284.jpg)

Dubbo-api-docs作为一个项目单独存在。

主要功能模块有两个：① 暴露服务 ② 扫描apiocs信息，缓存在本地 

主要注解：

@ApiModule：类注解，dubbo 接口模块信息，用于标注一个接口类模块的用途。

@ApiDoc：方法注解，dubbo 接口信息，用于标注一个接口的用途。

@RequestParam：类属性/方法参数注解，标注请求参数。

@ResponseProperty：类属性注解，标注响应参数。

## 1.2 学习dubbo 的 SPI 扩展机制

首先了解的是java spi的实现过程，但是javaspi在查找扩展类实现的试试吗，后会遍历spi配置文件并且将实现类全部加载然后实例化，在项目启动之初就会很耗时，不能实现按需加载实现类。

Dubbo spi 实现了按需加载实现类，并且 Dubbo SPI 除了可以按需加载实现类之外，增加了 IOC 和 AOP 的特性，还有个自适应扩展机制。

**spi****机制有几个重要的注解：**

1、@SPI注解，被此注解标记的接口，就表示是一个可扩展的接口，并标注默认值。
 2、@Adaptive注解，有两种注解方式：一种是注解在类上，一种是注解在方法上。
 3、@Activate注解，此注解需要注解在类上或者方法上，并注明被激活的条件，以及所有的被激活实现类中的排序信息

Spi的代码大体看了一下，但是还有一些地方没有看懂。后面会继续看源码做做笔记。

![img](https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images//clip_image004-1632657096284.jpg)

 

 

 

## 1.3 学习元数据中心

​    Dubbo元数据中心。主要功能都在dubbo-metadata中。

什么是元数据。服务治理中的元数据（Metadata）指的是服务分组、服务版本、服务名、方法列表、方法参数列表等。那么注册中心的信息主要包含了服务名，地址，等等。那么将元数据和服务信息分开。防止注册信息和元数据都耦合在一起。同时额方便了服务测试。降低了消费者获取服务提供者地址的时候获取大量无用的信息。

   学习的过程中，使用的注册中心是zookeeper，查看元数据的工具为ZooInspector

​             

![img](https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images/clip_image006-1632657096284.jpg)

## 1.4学习dubbo 服务暴露过程

这部分代码很多，也是dubbo核心代码的一部分，只学习了服务暴露的大体流程。部分源码分析在另外一个文件夹里。细节代码理解起来有些难度.大体的一个过程，以及哪些重要的方法和类了解了。在dbbo 2.7x中，元数据暴露的过程是在服务暴露完成之后进行的。

元数据暴露的一个 流程大概如下：

![img](https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images/clip_image008-1632657096284.jpg)

 

## 1.5 学习dubbo admin

## 1.6 api docs 和dubbo admin交互 

Apidocs的服务信息使用的方法实际自有两个

Apidocs暴露服务信息

1.所有的服务。包括服务下所有的接口。getBasicApiModuleInfo（）

2根据key 获取某个接口的所有详细信息。  apiParamsResponseInfo（key）

查询docs信息。其实返回的是参数信息。如果参数里面还有属性，是基本数据类型也包含进来，如果不是，就存入json格式的对象。查询docs信息 直接调用远程服务的方法返回。测试是保存参数信息，也是通过远程方法调用。

## 1.7 元数据中心如何和dubbo admin交互

发布metadata需要的url信息： metadataIdntifier ： serviceInterface（接口名称），version，group,side(提供者还是消费者)，application（项目名称） 用于构造zk的路径，data是zk节点上的

​           

Dubbo admin中对元数据信息。提供了元数据信息查询和测试的功能。ProviderService查询：获取所有的接口，在初始化的时候就向注册中心订阅并保存了。providerService.findByServices();获取某个方法的元数据信息，通过接口的MetadataIdentifier向元数据中心获取。

测试某个方法，先获取元数据信息，使用.findByService(service);获取。和获取接口信息一样。然后获取元数据方法的信息。然后通过递归构建每一个参数。把参数实列化，放入methodMetadata对象里面。

## 1.8 分析metadata和docs信息格式

docs信息包括：

1、服务信息，包括服务下的接口，缓存在apiModulesCache中

2、接口信息，包括接口的参数。还包括参数的属性 缓存在apiParamsAndRespCache中

使用的bean有

 1.服务信息 ModuleCacheItem 

 2.接口信息 ApiCacheUtem

 3.参数信息 ApiOaransCacheItem

 4.参数属性 ParamBean 

![img]https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images/clip_image010.jpg)

Metadata，元数据信息，主要是将一个接口的所有元数据信息都存储在一个model里面，这个model里同时也存储了很多字model进行嵌套，在元数据构建的时候进行实例化。

1.服务信息：ServiceDefinition

2.方法信息：MethodDefinition

3.类型信息：TypeDefinition

![img](https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images/clip_image012.jpg)

 

## 1.9 研究metadata和docs生成过程

两个数据的生成过程，看了代码，里面用了大量的递归来生成。

最多的知识点就是 使用了大量的反射的方法。还有一个重要的接口：Type。了Type接口作为Class，ParameterizedType，GenericArrayType，TypeVariable和WildcardType这几种类型的总的父接口。在程序中使用了很多type的子 接口来获取对象的类型，名字，原始类型，实际类型等等。

 

## 1.10分析dubbo admin 获取metadata和docs的代码

获取metadata是通过拼接url在注册中心里面获取。获取docs’ 是通过获取服务，作为消费者方，调用远程接口获取。zookeeper中的一个元数据中心的路径是 /metadata/服务名/版本/froup/provider/项目名称，对于获取apidocs信息是通过key来查询。key的格式为 接口全名称/参数名

## 1.11 研究服务查询和接口文档中对接口进行测试的代码

## 1.12创建demo 将api-docs-demo移动到 dubbo3.0

主要是将dubbo-api-docs中的dubbo-api-docs-examples 移动到dubbo-demo中

 

## 1.13将apidocs核心代码挪动到元数据中心

这部分主要是动手写代码

## 1.14 学习dubbo 3.0 云原生

![img](https://gitlab.summer-ospp.ac.cn/summer2021/210780568/-/raw/fd712cea68b293adf4af215b14ab59e32b925eb8/images/clip_image014.jpg) 服务端：

服务端发布全部应用服务；

将全部应用服务的元数据发布到内存中（存储在对象InMemoryWritableMetadataService）；

将dubbo的应用名发布到配置中心的“/dobbo/config/mapping/接口名/应用名”目录下（配置中心以zk为例，下同）；

将InMemoryWritableMetadataService对象发布为MetadataService服务，服务协议是dubbo；

将代表dubbo实例的ServiceInstance对象发布到注册中心的“/services/应用名/ServiceInstance的id属性值”目录下，ServiceInstance对象存储的数据有ip、应用名、MetadataService服务端口等；

消费端：

消费端根据引用服务的接口名从配置中心查找合适的应用名；

访问注册中心，找到应用名对应的ServiceInstance对象，从而得到MetadataService服务的连接地址，然后建立对注册中心的监听，当注册中心发生变化或者新增减少dubbo实例时，可重新执行下面第八和第九步；

访问服务端的MetadataService服务，得到服务端发布的所有服务元数据；

根据服务元数据建立与服务端的连接，后续直接进行服务调用。
