/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.dubbo.metadata.definition;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.apidocs.annotations.ApiDoc;
import org.apache.dubbo.apidocs.annotations.ApiModule;
import org.apache.dubbo.apidocs.annotations.RequestParam;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.config.spring.extension.SpringExtensionFactory;
import org.apache.dubbo.metadata.definition.model.*;
import org.apache.dubbo.metadata.definition.util.ClassUtils;

import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;
import org.apache.dubbo.metadata.definition.util.ClassTypeUtil;
import org.apache.dubbo.metadata.definition.model.Constants;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import static org.apache.dubbo.metadata.definition.model.Constants.ALLOWABLE_BOOLEAN_FALSE;
import static org.apache.dubbo.metadata.definition.model.Constants.ALLOWABLE_BOOLEAN_TRUE;
import static org.apache.dubbo.metadata.definition.model.Constants.METHOD_NAME_NAME;
import static org.apache.dubbo.metadata.definition.model.Constants.METHOD_PARAMETER_SEPARATOR;
import static org.apache.dubbo.metadata.definition.model.Constants.METHOD_PARAM_INDEX_BOUNDARY_LEFT;
import static org.apache.dubbo.metadata.definition.model.Constants.METHOD_PARAM_INDEX_BOUNDARY_RIGHT;
import static org.apache.dubbo.metadata.definition.model.Constants.NO_ARGS_NO_RETURN_VALUES_METHOD_DESC;
import static org.apache.dubbo.metadata.definition.model.Constants.SKIP_FIELD_SERIALVERSIONUID;
import static org.apache.dubbo.metadata.definition.model.Constants.SKIP_FIELD_THIS$0;

/**
 * 2015/1/27.
 */
public final class ServiceDefinitionBuilder {

    /**
     * Describe a Java interface in {@link ServiceDefinition}.
     *
     * @return Service description
     */
    public static ServiceDefinition build(final Class<?> interfaceClass) {
        ServiceDefinition sd = new ServiceDefinition();
        build(sd, interfaceClass);
        return sd;
    }

    public static FullServiceDefinition buildFullDefinition(final Class<?> interfaceClass) {
        FullServiceDefinition sd = new FullServiceDefinition();
        build(sd, interfaceClass);
        return sd;
    }

    public static FullServiceDefinition buildFullDefinition(final Class<?> interfaceClass, Map<String, String> params) {
        FullServiceDefinition sd = new FullServiceDefinition();
        build(sd, interfaceClass);
        sd.setParameters(params);
        return sd;
    }

    public static <T extends ServiceDefinition> void build(T sd, final Class<?> interfaceClass) {
        // ServiceLoader<interfaceClass>
        // 1. 是MetadataService 2. 有apiModule 3 没有apimodule
        Class<?> interclass = null;
        Set<ApplicationContext> set = SpringExtensionFactory.getContexts(); // 获取applicationContex
        ApplicationContext applicationContext = null;
        for (ApplicationContext applicationContext1 : set) applicationContext = applicationContext1;
        Method[] apiModuleMethods = null;
        Class<?> apiModuleClass = null;
        if (applicationContext != null) {
            Map<String, Object> apiModules = applicationContext.getBeansWithAnnotation(ApiModule.class);
            apiModuleClass = get(apiModules, interfaceClass); // 获取接口的实现类
            if (apiModuleClass != null) {
                apiModuleMethods = apiModuleClass.getMethods();
                sd.setModuleDocName(apiModuleClass.getAnnotation(ApiModule.class).value());
            }
        }
        if (apiModuleClass != null) {
            interclass = apiModuleClass; // 为空,表示没有
        } else {
            interclass = interfaceClass;
        }
        sd.setCanonicalName(interfaceClass.getCanonicalName());
        sd.setCodeSource(ClassUtils.getCodeSource(interclass));
        Annotation[] classAnnotations = interclass.getAnnotations();
        sd.setAnnotations(annotationToStringList(classAnnotations));

        TypeDefinitionBuilder builder = new TypeDefinitionBuilder();
        List<Method> methods = ClassUtils.getPublicNonStaticMethods(interclass);

        boolean async = false;
        String apiVersion = "";
        String apiGroup = "";
        if (interclass.isAnnotationPresent(Service.class)) {
            Service dubboService = apiModuleClass.getAnnotation(Service.class);
            async = dubboService.async();
            apiVersion = dubboService.version();
            apiGroup = dubboService.group();
        } else if (interclass.isAnnotationPresent(DubboService.class)) {
            DubboService dubboService = interclass.getAnnotation(DubboService.class);
            async = dubboService.async();
            apiVersion = dubboService.version();
            apiGroup = dubboService.group();
        }
        for (Method method : methods) {
            MethodDefinition md = new MethodDefinition();
            md.setName(method.getName());

            Annotation[] methodAnnotations = method.getAnnotations();
            md.setAnnotations(annotationToStringList(methodAnnotations));

            // Process parameter types.
            Class<?>[] paramTypes = method.getParameterTypes();
            Type[] genericParamTypes = method.getGenericParameterTypes();

            String[] parameterTypes = new String[paramTypes.length];
            for (int i = 0; i < paramTypes.length; i++) {
                TypeDefinition td = builder.build(genericParamTypes[i], paramTypes[i]);
                // parameterTypes[i] = td.getType();
                parameterTypes[i] = genericParamTypes[i].getTypeName();
            }

            md.setParameterTypes(parameterTypes);

            // Process return type.
            TypeDefinition td = builder.build(method.getGenericReturnType(), method.getReturnType());
            md.setReturnType(method.getGenericReturnType().getTypeName());
            //   md.setReturnType(td.getType());

            sd.getMethods().add(md);
            ApiCacheItem apiCacheItem = new ApiCacheItem();
            md.setApiCacheItem(apiCacheItem);
            if (apiModuleMethods != null)
                for (Method method1 : apiModuleMethods) {
                    if (method1.getName() == method.getName() && method1.isAnnotationPresent(ApiDoc.class)) {
                        md.setApiCacheItem(apiCacheItem);
                        ApiModule moduleAnn = apiModuleClass.getAnnotation(ApiModule.class);
                        processApiDocAnnotation(method1, apiCacheItem, moduleAnn, async, apiVersion, apiGroup);
                        break;
                    }
                }
        }
        // process apidocs

        sd.setTypes(builder.getTypeDefinitions());
    }

    private static void processApiDocAnnotation(Method method, ApiCacheItem apiCacheItem, ApiModule moduleAnn, boolean async, String apiVersion, String apiGroup) {

        ApiDoc dubboApi = method.getAnnotation(ApiDoc.class);
        Class<?>[] argsClass = method.getParameterTypes();
        Annotation[][] argsAnns = method.getParameterAnnotations();
        Parameter[] parameters = method.getParameters();
        Type[] parametersTypes = method.getGenericParameterTypes();
        List<ApiParamsCacheItem> paramList = new ArrayList<>(argsClass.length);
        apiCacheItem.setAsync(async);
        apiCacheItem.setApiName(method.getName());
        apiCacheItem.setApiDocName(dubboApi.value());
        apiCacheItem.setApiVersion(apiVersion);
        apiCacheItem.setApiGroup(apiGroup);
        apiCacheItem.setApiRespDec(dubboApi.responseClassDescription());
        apiCacheItem.setDescription(dubboApi.description());
        apiCacheItem.setApiModelClass(moduleAnn.apiInterface().getCanonicalName());
        apiCacheItem.setParams(paramList);
        apiCacheItem.setResponse(ClassTypeUtil.calss2Json(method.getGenericReturnType(), method.getReturnType()));
        StringBuilder methodParamInfoSb = new StringBuilder();
        for (int i = 0; i < argsClass.length; i++) {
            Class<?> argClass = argsClass[i];
            Type parameterType = parametersTypes[i];
            methodParamInfoSb.append(METHOD_PARAM_INDEX_BOUNDARY_LEFT).append(i)
                .append(METHOD_PARAM_INDEX_BOUNDARY_RIGHT).append(argClass.getCanonicalName());
            if (i + 1 < argsClass.length) {
                methodParamInfoSb.append(METHOD_PARAMETER_SEPARATOR);
            }
            Annotation[] argAnns = argsAnns[i];
            ApiParamsCacheItem paramListItem = new ApiParamsCacheItem();
            paramList.add(paramListItem);
            paramListItem.setParamType(argClass.getCanonicalName());
            paramListItem.setParamIndex(i);
            RequestParam requestParam = null;
            // Handling @RequestParam annotations on parameters
            for (Annotation ann : argAnns) {
                if (ann instanceof RequestParam) {
                    requestParam = (RequestParam) ann;
                }
            }
            ParamBean paramBean = processHtmlType(argClass, requestParam, null);
            Parameter methodParameter = parameters[i];
            if (paramBean == null) {
                // Not a basic type, handling properties in method parameters
                List<ParamBean> apiParamsList = processField(argClass, parameterType, methodParameter);
                if (apiParamsList != null && !apiParamsList.isEmpty()) {
                    paramListItem.setParamInfo(apiParamsList);
                }
            } else {
                // Is the basic type
                paramListItem.setName(methodParameter.getName());
                paramListItem.setHtmlType(paramBean.getHtmlType().name());
                paramListItem.setAllowableValues(paramBean.getAllowableValues());
                if (requestParam != null) {
                    // Handling requestparam annotations on parameters
                    paramListItem.setDocName(requestParam.value());
                    paramListItem.setDescription(requestParam.description());
                    paramListItem.setExample(requestParam.example());
                    paramListItem.setDefaultValue(requestParam.defaultValue());
                    paramListItem.setRequired(requestParam.required());
                } else {
                    paramListItem.setRequired(false);
                }
                if (HtmlTypeEnum.TEXT_AREA.name().equals(paramListItem.getHtmlType())) {
                    List<ParamBean> apiParamsList = processField(argClass, parameterType, methodParameter);
                    paramListItem.setAllowableValues(apiParamsList.get(0).getAllowableValues());
                    paramListItem.setSubParamsJson(apiParamsList.get(0).getSubParamsJson());
                }
            }
        }
        apiCacheItem.setMethodParamInfo(methodParamInfoSb.toString());
    }

    /**
     * For the attributes in the method parameters, only one layer is processed.
     * The deeper layer is directly converted to JSON, and the deeper layer is up to 5 layers
     */
    private static List<ParamBean> processField(Class<?> argClass, Type parameterType, Parameter parameter) {
        Map<String, String> genericTypeAndNamesMap;
        if (parameterType instanceof ParameterizedTypeImpl) {
            ParameterizedTypeImpl parameterTypeImpl = (ParameterizedTypeImpl) parameterType;
            TypeVariable<? extends Class<?>>[] typeVariables = parameterTypeImpl.getRawType().getTypeParameters();
            Type[] actualTypeArguments = parameterTypeImpl.getActualTypeArguments();
            genericTypeAndNamesMap = new HashMap<>(typeVariables.length);
            for (int i = 0; i < typeVariables.length; i++) {
                genericTypeAndNamesMap.put(typeVariables[i].getTypeName(), actualTypeArguments[i].getTypeName());
            }
        } else {
            genericTypeAndNamesMap = Collections.EMPTY_MAP;
        }

        List<ParamBean> apiParamsList = new ArrayList<>(16);
        // get all fields
        List<Field> allFields = ClassTypeUtil.getAllFields(null, argClass);
        if (allFields.size() > 0) {
            for (Field field : allFields) {
                if (SKIP_FIELD_SERIALVERSIONUID.equals(field.getName()) || SKIP_FIELD_THIS$0.equals(field.getName())) {
                    continue;
                }
                ParamBean paramBean = new ParamBean();
                paramBean.setName(field.getName());
                String genericTypeName = genericTypeAndNamesMap.get(field.getGenericType().getTypeName());
                Class<?> genericType = null;
                if (StringUtils.isBlank(genericTypeName)) {
                    paramBean.setJavaType(field.getType().getCanonicalName());
                } else {
                    paramBean.setJavaType(genericTypeName);
                    genericType = ClassTypeUtil.makeClass(genericTypeName);
                }
                RequestParam requestParam = null;
                if (field.isAnnotationPresent(RequestParam.class)) {
                    // Handling @RequestParam annotations on properties
                    requestParam = field.getAnnotation(RequestParam.class);
                    paramBean.setDocName(requestParam.value());
                    paramBean.setRequired(requestParam.required());
                    paramBean.setDescription(requestParam.description());
                    paramBean.setExample(requestParam.example());
                    paramBean.setDefaultValue(requestParam.defaultValue());
                } else {
                    paramBean.setRequired(false);
                }

                ParamBean tempParamBean = processHtmlType(null == genericType ?
                    field.getType() : genericType, requestParam, paramBean);
                if (tempParamBean == null || HtmlTypeEnum.TEXT_AREA.equals(tempParamBean.getHtmlType())) {
                    Object objResult;
                    if (null == genericType) {
                        objResult = ClassTypeUtil.initClassTypeWithDefaultValue(
                            field.getGenericType(), field.getType(), 0, genericTypeAndNamesMap);
                    } else {
                        objResult = ClassTypeUtil.initClassTypeWithDefaultValue(
                            ClassTypeUtil.makeParameterizedType(genericTypeName), genericType, 0,
                            true, genericTypeAndNamesMap);
                    }
                    if (!ClassTypeUtil.isBaseType(objResult)) {
                        paramBean.setHtmlType(HtmlTypeEnum.TEXT_AREA);
                        paramBean.setSubParamsJson(JSON.toJSONString(objResult, ClassTypeUtil.FAST_JSON_FEATURES));
                    }
                }
                apiParamsList.add(paramBean);
            }
        } else {
            ParamBean paramBean = new ParamBean();
            paramBean.setName(parameter.getName());
            paramBean.setJavaType(argClass.getCanonicalName());
            RequestParam requestParam = null;
            if (parameter.isAnnotationPresent(RequestParam.class)) {
                // Handling @RequestParam annotations on properties
                requestParam = parameter.getAnnotation(RequestParam.class);
                paramBean.setDocName(requestParam.value());
                paramBean.setRequired(requestParam.required());
                paramBean.setDescription(requestParam.description());
                paramBean.setExample(requestParam.example());
                paramBean.setDefaultValue(requestParam.defaultValue());
            } else {
                paramBean.setRequired(false);
            }

            Object objResult = ClassTypeUtil.initClassTypeWithDefaultValue(
                parameterType, argClass, 0, genericTypeAndNamesMap);
            if (!ClassTypeUtil.isBaseType(objResult)) {
                paramBean.setHtmlType(HtmlTypeEnum.TEXT_AREA);
                paramBean.setSubParamsJson(JSON.toJSONString(objResult, ClassTypeUtil.FAST_JSON_FEATURES));
            }
            apiParamsList.add(paramBean);
        }
        return apiParamsList;
    }


    /**
     * Determine what HTML form elements to use.
     *
     * @param classType  classType
     * @param annotation annotation
     * @param param      param
     * @return org.apache.dubbo.apidocs.core.beans.ParamBean
     */
    private static ParamBean processHtmlType(Class<?> classType, RequestParam annotation, ParamBean param) {
        if (param == null) {
            param = new ParamBean();
        }
        if (annotation != null) {
            param.setAllowableValues(annotation.allowableValues());
        }
        // Is there any allowed values
        boolean hasAllowableValues = (param.getAllowableValues() != null && param.getAllowableValues().length > 0);
        // Processed or not
        boolean processed = false;
        if (Integer.class.isAssignableFrom(classType) || int.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.NUMBER_INTEGER);
            processed = true;
        } else if (Byte.class.isAssignableFrom(classType) || byte.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.TEXT_BYTE);
            processed = true;
        } else if (Long.class.isAssignableFrom(classType) || long.class.isAssignableFrom(classType) ||
            BigDecimal.class.isAssignableFrom(classType) || BigInteger.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.NUMBER_INTEGER);
            processed = true;
        } else if (Double.class.isAssignableFrom(classType) || double.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.NUMBER_DECIMAL);
            processed = true;
        } else if (Float.class.isAssignableFrom(classType) || float.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.NUMBER_DECIMAL);
            processed = true;
        } else if (String.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.TEXT);
            processed = true;
        } else if (Character.class.isAssignableFrom(classType) || char.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.TEXT_CHAR);
            processed = true;
        } else if (Short.class.isAssignableFrom(classType) || short.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.NUMBER_INTEGER);
            processed = true;
        } else if (Date.class.isAssignableFrom(classType) || LocalDateTime.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.DATETIME_SELECTOR);
            processed = true;
        } else if (LocalDate.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.DATE_SELECTOR);
            processed = true;
        } else if (classType.isArray() || Collection.class.isAssignableFrom(classType) ||
            Map.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.TEXT_AREA);
            processed = true;
        }

        if (processed) {
            // Processed, time to return
            if (hasAllowableValues) {
                // Allowed values has value, change to select
                param.setHtmlType(HtmlTypeEnum.SELECT);
            }
            return param;
        }

        // haven't dealt with it. Go on
        if (Boolean.class.isAssignableFrom(classType) || boolean.class.isAssignableFrom(classType)) {
            param.setHtmlType(HtmlTypeEnum.SELECT);
            // Boolean can only be true / false. No matter what the previous allowed value is, it is forced to replace
            param.setAllowableValues(new String[]{ALLOWABLE_BOOLEAN_TRUE, ALLOWABLE_BOOLEAN_FALSE});
            processed = true;
        } else if (Enum.class.isAssignableFrom(classType)) {
            // process enum
            param.setHtmlType(HtmlTypeEnum.SELECT);

            Object[] enumConstants = classType.getEnumConstants();
            String[] enumValues = new String[enumConstants.length];
            try {
                Method getNameMethod = classType.getMethod(METHOD_NAME_NAME);
                for (int i = 0; i < enumConstants.length; i++) {
                    Object obj = enumConstants[i];
                    enumValues[i] = (String) getNameMethod.invoke(obj);
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                //LOG.error(e.getMessage(), e);
            }

            if (!hasAllowableValues) {
                // If there is no optional value, it is taken from the enumeration.
                param.setAllowableValues(enumValues);
            } else {
                // If there has allowable values, it is necessary to check whether the allowable values matches the enumeration.
                boolean checkSuccess = true;
                String[] allowableValues = param.getAllowableValues();
                for (String allowableValue : allowableValues) {
                    for (String enumValue : enumValues) {
                        if (!StringUtils.equals(enumValue, allowableValue)) {
                            checkSuccess = false;
                        }
                    }
                }
                if (!checkSuccess) {
                    //  LOG.error("The allowed value in the @RequestParam annotation does not match the " +
                    //  "annotated enumeration " + classType.getCanonicalName() + ", please check!");
                }
            }
            processed = true;
        }
        if (processed) {
            return param;
        }
        return null;
    }

    /**
     * @param apiModules     有apidocs注解的
     * @param interfaceClass
     * @return
     */
    private static Class<?> get(Map<String, Object> apiModules, Class<?> interfaceClass) {
        for (String key : apiModules.keySet()) {
            Class<?> apiModuleClass = apiModules.get(key).getClass();
            ApiModule moduleAnn = apiModuleClass.getAnnotation(ApiModule.class);
            if (moduleAnn.apiInterface().getCanonicalName() == interfaceClass.getCanonicalName()) {
                return apiModuleClass;
            }
        }
        return null;
    }

    private static List<String> annotationToStringList(Annotation[] annotations) {
        if (annotations == null) {
            return Collections.emptyList();
        }
        List<String> list = new ArrayList<>();
        for (Annotation annotation : annotations) {
            list.add(annotation.toString());
        }
        return list;
    }

    /**
     * Describe a Java interface in Json schema.
     *
     * @return Service description
     */
    public static String schema(final Class<?> clazz) {
        ServiceDefinition sd = build(clazz);
        Gson gson = new Gson();
        return gson.toJson(sd);
    }

    private ServiceDefinitionBuilder() {
    }
}


