package org.apache.dubbo.apidocs.examples.api.impl;

import org.apache.dubbo.apidocs.examples.api.GenericTest;
import org.apache.dubbo.apidocs.examples.params.GenericBean;
import org.apache.dubbo.apidocs.examples.params.QuickStartRequestBase;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService(async = true)
public class GenericTestImpl implements GenericTest {

    @Override
    public String api1(GenericBean genericBean) {
        return "卧槽";
    }

    @Override
    public String api2(QuickStartRequestBase quickStartRequestBase) {
        return "wocao";
    }
}
