package org.apache.dubbo.apidocs.examples.api;

import org.apache.dubbo.apidocs.examples.params.GenericBean;
import org.apache.dubbo.apidocs.examples.params.QuickStartRequestBase;

public interface GenericTest {
    public  String  api1(GenericBean  genericBean);
    public  String api2(QuickStartRequestBase  quickStartRequestBase);
}
